import {
  Rule,
  SchematicContext,
  Tree,
  SchematicsException,
  apply,
  url,
  template, move, mergeWith, chain
} from '@angular-devkit/schematics';
import { Schema } from './schema';
import { buildDefaultPath } from "@schematics/angular/utility/project";
//import { parseName } from "@schematics/angular/utility/parse-name";
import { strings } from '@angular-devkit/core';

import {
  //addModuleImportToRootModule,
  addPackageJsonDependency,
  getProjectFromWorkspace,
  getWorkspace,
  NodeDependency,
  NodeDependencyType,
  //WorkspaceProject,
  //addDeclarationToModule,
  addImportToModule,
  getAppModulePath,
  getSourceFile,
  Change,
  addDeclarationToModule,
  //addProviderToModule,
  InsertChange,
  getSourceNodes,
  insertImport,
  applyLintFix,
  // addDeclarationToModule,
  // addProviderToModule
} from 'schematics-utilities';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import * as ts from 'typescript';



// You don't have to export the function as default. You can also have more than one rule factory
// per file.
export function authentication(_options: Schema): Rule {
  return (host: Tree, context: SchematicContext) => {

    context.logger.log('info', 'Starting modification...');

    const workspaceConfigBuffer: Buffer | null = host.read("angular.json");
    if (!workspaceConfigBuffer) {
      throw new SchematicsException("Not an Angular CLI workspace");
    }

    const workspaceConfig = JSON.parse(workspaceConfigBuffer.toString());
    const projectName: string = _options.project || workspaceConfig.defaultProject;
    const project = workspaceConfig.projects[projectName];

    const baseProjectPath = buildDefaultPath(project);

    return chain([
      addServices(_options, baseProjectPath),
      addAuthenticationComponents(_options, baseProjectPath),
      addSampleComponent(_options, baseProjectPath),
      addToModule(_options),
      addToRoutes(_options, baseProjectPath),
      configureBuildAndServe(_options, baseProjectPath),
      addPackageJsonDependencies(),
      installPackageJsonDependencies(),
      logFinished()
    ]);
  };
}


export function addServices(_options: Schema, baseProjectPath: string): Rule {
  return (host: Tree, context: SchematicContext) => {

    context.logger.log('info', 'Adding authentication service, guard and interceptor...');

    const authenticationServicesPath = baseProjectPath + '/services/authentication';
    const sourceTemplate = url('./files/services');
    const sourceParameterizedTemplate = apply(sourceTemplate, [
      template({
        ..._options,
        ...strings
      }),
      move(authenticationServicesPath)
    ]);

    const rule = mergeWith(sourceParameterizedTemplate);

    return rule(host, context);
  }
}

export function addAuthenticationComponents(_options: Schema, baseProjectPath: string): Rule {
  return (host: Tree, context: SchematicContext) => {

    context.logger.log('info', 'Adding authentication components...');

    const authenticationComponentsPath = baseProjectPath + '/components/authentication';
    const sourceTemplate = url('./files/components/authentication');
    const sourceParameterizedTemplate = apply(sourceTemplate, [
      template({
        ..._options,
        ...strings
      }),
      move(authenticationComponentsPath)
    ]);

    const rule = mergeWith(sourceParameterizedTemplate);

    return rule(host, context);
  };
}

export function addSampleComponent(_options: Schema, baseProjectPath: string): Rule {
  return (host: Tree, context: SchematicContext) => {

    context.logger.log('info', 'Adding sample component...');

    const sampleComponentPath = baseProjectPath + '/components/sample';
    const sourceTemplate = url('./files/components/sample');

    const sourceParameterizedTemplate = apply(sourceTemplate, [
      template({
        ..._options,
        ...strings
      }),
      move(sampleComponentPath)
    ]);

    const rule = mergeWith(sourceParameterizedTemplate);

    return rule(host, context);
  };
}



function addPackageJsonDependencies(): Rule {

  return (host: Tree, context: SchematicContext) => {
    const dependencies: NodeDependency[] = [
      { type: NodeDependencyType.Default, version: '^10.0.2', name: 'angular-auth-oidc-client' },
      { type: NodeDependencyType.Default, version: '^1.0.57', name: '@attache/messaging' },
    ];

    dependencies.forEach(dependency => {
      addPackageJsonDependency(host, dependency);
      context.logger.log('info', `Added "${dependency.name}" into ${dependency.type}`);
    });

    return host;
  };
}

function installPackageJsonDependencies(): Rule {
  return (host: Tree, context: SchematicContext) => {
    context.addTask(new NodePackageInstallTask());
    context.logger.log('info', `Installing packages...`);

    return host;
  };
}

function addToModule(options: Schema): Rule {
  return (host: Tree, context: SchematicContext) => {

    context.logger.log('info', `Adding stuff to app module:`);

    const workspace = getWorkspace(host);
    const project = getProjectFromWorkspace(
      workspace,
      // Takes the first project in case it's not provided by CLI
      options.project ? options.project : Object.keys(workspace['projects'])[0]
    );

    const targets = (<any>project).targets || project.architect;
    const modulePath = getAppModulePath(host, targets.build.options.main);
    const moduleSource = getSourceFile(host, modulePath);

    let changes = [
      ...addDeclarations(moduleSource, modulePath, context),
      ...addImports(moduleSource, modulePath, options, context),
      ...addProviders(moduleSource, modulePath, context),
      ...addModuleConstructor(moduleSource, modulePath, context)
    ] as Change[];

    const recorder = host.beginUpdate(modulePath);

    changes.forEach(change => {
      if (change instanceof InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      }
    });
    host.commitUpdate(recorder);

    return host;
  };
}

function addDeclarations(source: ts.SourceFile, modulePath: string, context: SchematicContext): Change[] {
  context.logger.log('info', 'Adding declarations...');
  return [
    ...addDeclarationToModule(source, modulePath, 'RedirectComponent', './components/authentication/redirect.component'),
    ...addDeclarationToModule(source, modulePath, 'UnauthorizedComponent', './components/authentication/unauthorized.component'),
    ...addDeclarationToModule(source, modulePath, 'SampleComponent', './components/sample/sample.component')
  ];
}

function addImports(source: ts.SourceFile, modulePath: string, options: Schema, context: SchematicContext): Change[] {
  context.logger.log('info', 'Adding imports...');
  return [
    ...addImportToModule(source, modulePath, 'AuthModule.forRoot()', 'angular-auth-oidc-client'),
    ...addImportToModule(source, modulePath, 'HttpClientModule', '@angular/common/http'),
    ...addImportToModule(source, modulePath, `AttacheBusModule.ForApi('${options.defaultGateway}')`, '@attache/messaging')
  ];
}

// Cant use addProviderToModule because of some bug (possible caused because the array is empty to begin with)
function addProviders(source: ts.SourceFile, modulePath: string, context: SchematicContext): Change[] {
  context.logger.log('info', 'Adding providers...');
  const node = getArrayLiteralNode(source, 'providers'); 
  let pos = node.getStart() + 1;

  return [
    new InsertChange(modulePath, pos, '\n\t\tAttacheCommandQueryBus,\n') as Change,
    new InsertChange(modulePath, pos, '\t\tOidcSecurityService,\n'),
    new InsertChange(modulePath, pos, '\t\tOidcConfigService,\n'),
    new InsertChange(modulePath, pos, '\t\tAuthenticationService,\n'),
    new InsertChange(modulePath, pos, '\t\tAuthenticationInterceptor\n')
  ].concat([
    insertImport(source, modulePath, 'AttacheCommandQueryBus', '@attache/messaging', false),
    insertImport(source, modulePath, 'OidcSecurityService', 'angular-auth-oidc-client', false),
    insertImport(source, modulePath, 'OidcConfigService', 'angular-auth-oidc-client', false),
    insertImport(source, modulePath, 'AuthenticationService', './services/authentication/authentication.service', false),
    insertImport(source, modulePath, 'AuthenticationInterceptor', './services/authentication/authentication.interceptor', false)
  ]);
}

function addModuleConstructor(source: ts.SourceFile, modulePath: string, context: SchematicContext) {
  context.logger.log('info', 'Adding module constructor...');
  
  const nodes = getSourceNodes(source);
  const appModuleClass = nodes.filter((n: ts.Node) =>
   (n.kind === ts.SyntaxKind.Identifier || n.kind === ts.SyntaxKind.LastBinaryOperator ) && n.getText() === 'AppModule')[0];

  let siblings = appModuleClass.parent.getChildren();
  const curlyNodeIndex = siblings.findIndex(n => n.kind === ts.SyntaxKind.FirstPunctuation);
  siblings = siblings.slice(curlyNodeIndex);
  const listNode = siblings.find(n => n.kind === ts.SyntaxKind.SyntaxList || n.kind === ts.SyntaxKind.LastJSDocTagNode);
  if(!listNode)
    throw new SchematicsException('Something went wrong when adding constructor');


  let toAdd = `constructor(
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.initialise();
  }`;

  return [new InsertChange(modulePath, listNode.pos + 1, toAdd)];

}

function addToRoutes(options: any, baseProjectPath: string): Rule {
  return (host: Tree, context: SchematicContext) => {

    context.logger.log('info', 'Configuring routes...');

    const routingPath = baseProjectPath + '/app-routing.module.ts';
    const source = getSourceFile(host, routingPath);

    let changes = [
      insertImport(source, routingPath, 'RedirectComponent', './components/authentication/redirect.component', false),
      insertImport(source, routingPath, 'UnauthorizedComponent', './components/authentication/unauthorized.component', false),
      insertImport(source, routingPath, 'SampleComponent', './components/sample/sample.component', false),
      insertImport(source, routingPath, 'AuthenticationGuard', './services/authentication/authentication.guard', false)
    ];

    const redirectRoute = `{
      path: 'redirect',
      component: RedirectComponent
    }`;

    const unauthorizedRoute = `{
      path: 'unauthorized',
      component: UnauthorizedComponent
    }`;

    const sampleRoute = `{
      path: '',
      component: SampleComponent,
      canActivate: [AuthenticationGuard]
    }`;

    const routesNode = getArrayLiteralNode(source, 'routes');
    let pos = routesNode.getStart() + 1;
    const recorder = host.beginUpdate(routingPath);

    recorder.insertRight(pos, `\t${redirectRoute},\n`);
    recorder.insertRight(pos, `\t${unauthorizedRoute},\n`);
    recorder.insertRight(pos, `\t${sampleRoute}\n`);

    changes.forEach(change => {
      if (change instanceof InsertChange) {
        recorder.insertLeft(change.pos, change.toAdd);
      }
    });

    host.commitUpdate(recorder);

    return host;
  };
}

function configureBuildAndServe(options: Schema, baseProjectPath: string): Rule {
  return (host: Tree, context: SchematicContext) => {
    
    const angularJsonBuffer = host.read('/angular.json');
    const packageJsonBuffer = host.read('/package.json');

    if(angularJsonBuffer === null || packageJsonBuffer === null)
      throw new SchematicsException('Could not find angular.json or package.json');

    let angularJson = JSON.parse(angularJsonBuffer.toString());
    
    angularJson.projects[Object.keys(angularJson.projects)[0] as string].architect.serve.options.port = parseInt(options.servePort.toString()); //yeah I know

    let packageJson = JSON.parse(packageJsonBuffer.toString());

    let aspNetBuildHostingOptions = options.baseUrlPath ? 
      `--base-href=/${options.baseUrlPath}/ --deploy-url=/${options.baseUrlPath}/` : 
    `--base-href=/ --deploy-url=/`;
    let startDockerHostingOptions = options.baseUrlPath ? 
      `--base-href=/${options.baseUrlPath}/ --deploy-url=/${options.baseUrlPath}/ --serve-path /${options.baseUrlPath}/ --public-host=http://${options.subdomain}.localtest.me/${options.baseUrlPath}` : 
    `--base-href=/ --deploy-url=/ --serve-path / --public-host=http://${options.subdomain}.localtest.me`;

    packageJson.scripts['start-docker'] = `ng serve --host 0.0.0.0 --disable-host-check ${startDockerHostingOptions}`;
    
    if(options.pathToWWWRoot)
      packageJson.scripts['aspnet-build'] = `ng build --watch=true --outputPath ${options.pathToWWWRoot} ${aspNetBuildHostingOptions}`;

    host.overwrite('/package.json', JSON.stringify(packageJson, null, 2));
    host.overwrite('/angular.json', JSON.stringify(angularJson, null , 2));

    return host;
  };
}

function logFinished(): Rule {
  return (host: Tree, context: SchematicContext) => {
    
    context.logger.log('info', `Modifications complete!`);

    return host;
  };
}

function getArrayLiteralNode(source: ts.SourceFile, name: string) {
  const nodes = getSourceNodes(source);

  const arrayNode = nodes.filter((n: ts.Node) => (n.getChildren().findIndex(c => (c.getText() === name)) !== -1))
  .map((n: ts.Node) => {
    const arrNodes = n.getChildren().filter(c => (c.kind = ts.SyntaxKind.ArrayLiteralExpression));
    return arrNodes[arrNodes.length - 1];
  })[0] as ts.ArrayLiteralExpression;

  if(arrayNode === null)
    throw new SchematicsException('Could not find array node with name ' + name);

  return arrayNode;
}

function printAllChildren(node: ts.Node, depth = 0) {
  console.log(new Array(depth + 1).join('----'), syntaxKindToName(node.kind), `<<<<${node.getText()}>>>>`);
  depth++;
  node.getChildren().forEach(c => printAllChildren(c, depth));
}

function syntaxKindToName(kind: ts.SyntaxKind) {
  return (<any>ts).SyntaxKind[kind];
}
