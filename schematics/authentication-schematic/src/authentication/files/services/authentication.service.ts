import { Injectable } from '@angular/core';

import {
  OidcSecurityService,
  OpenIdConfiguration,
  AuthWellKnownEndpoints,
  AuthorizationState } from 'angular-auth-oidc-client';

import { Observable } from 'rxjs';
import { map, take, switchMap, tap, filter } from 'rxjs/operators';
import { AttacheCommandQueryBus } from '@attache/messaging';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(
    private oidcSecurityService: OidcSecurityService,
    private commandQueryBus: AttacheCommandQueryBus,
  ) { }

  private thisBaseUrl(): string {
    return location.protocol + '//' + location.host;
  }

  private idBaseUrl(): string {
    return location.protocol + '//' + location.host.replace('<%= subdomain %>', 'id');
  }

  public get loggedIn(): Observable<boolean> {
    return this.oidcSecurityService.getIsAuthorized();
  }

  public logOut() {
    this.oidcSecurityService.logoff();
  }

  public logIn() {
    this.oidcSecurityService.authorize();
  }

  public initialise() {
    const config = this.getOpenIdConfiguration();
    const authWellKnownEndpoints = this.getAuthWellKnownEndpoints();

    this.oidcSecurityService.setupModule(config, authWellKnownEndpoints);

    this.oidcSecurityService.onCheckSessionChanged.subscribe(loggedIn => this.logOut());
  }

  public completeAuthentication(): Observable<AuthorizationState> {
    return this.oidcSecurityService.getIsModuleSetup().pipe(
      filter((isSetup: boolean) => isSetup),
      take(1),
      tap(() => this.oidcSecurityService.authorizedCallbackWithCode(window.location.toString())),
      switchMap(() => this.oidcSecurityService.onAuthorizationResult),
      take(1),
      map(r => r.authorizationState)
    );
  }

  private getOpenIdConfiguration(): OpenIdConfiguration {
    return {
      stsServer: this.idBaseUrl(),
      redirect_url: this.thisBaseUrl() + <% if(baseUrlPath) {  %> '/<%= baseUrlPath %>/redirect' <% } else { %> '/redirect' <% } %>,
      client_id: '<%= clientId %>',
      response_type: 'code',
      scope: '<%= requestedScopes %>', //
      post_logout_redirect_uri: this.thisBaseUrl() + <% if(baseUrlPath) {  %> '/<%= baseUrlPath %>/' <% } else { %> '/' <% } %>,
      post_login_route: '/',
      log_console_warning_active: true,
      max_id_token_iat_offset_allowed_in_seconds: 30,
      start_checksession: true,
    };
  }

  private getAuthWellKnownEndpoints(): AuthWellKnownEndpoints {
    return {
      jwks_uri: this.idBaseUrl() + '/.well-known/openid-configuration/jwks',
      issuer: this.idBaseUrl(),
      authorization_endpoint: this.idBaseUrl() + '/connect/authorize',
      token_endpoint: this.idBaseUrl() + '/connect/token',
      userinfo_endpoint: this.idBaseUrl() + '/connect/userinfo',
      end_session_endpoint: this.idBaseUrl() + '/connect/endsession',
      check_session_iframe: this.idBaseUrl() + '/connect/checksession',
      revocation_endpoint: this.idBaseUrl() + '/connect/revocation',
      introspection_endpoint: this.idBaseUrl() + '/connect/introspect',
    };
  }
}

