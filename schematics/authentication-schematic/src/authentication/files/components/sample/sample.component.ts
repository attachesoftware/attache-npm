import { Component, OnInit } from '@angular/core';
import { AttacheCommandQueryBus } from '@attache/messaging';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {

  constructor(
    private bus: AttacheCommandQueryBus,
    private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    // Execute commands/queries here and watch you token get magically added to the request!
  }

  logout() {
    this.authenticationService.logOut();
  }

}
