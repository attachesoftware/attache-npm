import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-redirect',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss']
})
export class RedirectComponent implements OnInit {

  public message = '';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) {}

  ngOnInit() {
    this.authenticationService.completeAuthentication().subscribe(authState => {
      if (authState === 'forbidden') {
        this.message = 'Forbidden';
      } else if (authState === 'unauthorized') {
        this.message = 'Unauthorized';
      } else if (authState === 'authorized') {
        this.router.navigate(['/']);
      }
    });
  }


}
