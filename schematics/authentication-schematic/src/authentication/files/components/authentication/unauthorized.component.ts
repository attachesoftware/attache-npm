import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';

@Component({
  selector: 'app-unauthorized',
  templateUrl: './shared.component.html',
  styleUrls: ['./shared.component.scss']
})
export class UnauthorizedComponent implements OnInit {

  public message = '';

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.logIn();
  }

}
