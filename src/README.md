# Attache Messaging Library
Welcome to the Attache Messaging Library

## Prerequisites 
This library was set up to send commands and queries that take the following structure:
### Command
```c#
public class CommandPayload
{
	public string CommandType { get; set; }
	public string Data { get; set; }
}
```
### Query
```c#
public class QueryPayload
{
	public string QueryType { get; set; }
	public string ResponseType { get; set; }
	public string Data { get; set; }
}
```
Where Data is the json encoded string of whatever the contents of your command actually is i.e. the ICommand/IQuery. If your commands and queries do not follow this model then this library will not work for you. 

### Endpoints

The endpoint the commands and queries will be sent to must also take the following form: `https://gateway.<environment>/api/<yourContainer>/Command` where environment is localtest.me or bronze.aws.attachecloud.com or whatever, and yourContainer is the container where the command/query is handled e.g. payroll.

## Installation
After installing the above dependencies, install `gel-core` via:
```shell
npm install --save @attache/messaging
```
Once installed you need to import our main module:
```js
import {AttacheBusModule} from '@attache/messaging';

// add it to your imports //

@NgModule({
    declarations: [
		// ...
    ],
    imports: [
        //...
		// the parameter you pass in is for the api path, this for example will use /api/payroll/Query and /api/payroll/Command to execute against.		
        AttacheBusModule.ForApi("payroll") // This can be overriden if you want to send a command to a different back-end container
		//...
    ],
    providers: [CommandQueryBus, SpinnerService],
    bootstrap: [AppComponent]
})


```
## Setup Option 1: Typed command and query names

After you have imported successfully you will need to setup the bus for your application. You need to define, a Query type, a Query Response type, Command Type, and Callback Type. The types below are sort of like enums, a function that takes one of these types as a parameter will only accept string literals that belong to this type. This gives a bit of extra type safety for these commands/queries.

It is a bit of work to set these types up but they are helpful once you have them.

```js

export type PayrollQuery = "GetPayroll" | "GetEmployee"  ;
export type PayrollQueryResponses = "PayrollResponse" | "EmployeeResponse" ;
export type PayrollCommands = "SubmitPayroll" | "EditEmployee";
export type PayrollCallbacks =  "PayrollSubmissionInProgress" | "PayrollSubmitionComplete" | "EmployeeModified"


```
After you have defined you commands and queries you will need to create your bus 
```js
export class PayrollBus extends AttacheCommandQueryBus<PayrollQuery, PayrollQueryResponses, PayrollCommands, PayrollCallbacks> {}

```
This isn't needed but it is helpful, the difference looks like this
```js

class PayrollComponent {
	constructor(private bus:AttacheCommandQueryBus<PayrollQuery, PayrollQueryResponses, PayrollCommands, PayrollCallbacks>) {}
}
// vs //
class PayrollComponent {
	constructor(private bus:PayrollBus) {}
}
```

## Setup Option 2: Regular strings for command and query names

Alternatively, if you don't want to follow the steps above you can skip the creation of the string literal types and instantiate the bus without them:

```js
class PayrollComponent {
	constructor(private bus:AttacheCommandQueryBus) {}
}
```
This will cause the generic parameters to take a default value of string, so you can pass any old string for the command/query names.

## Executing commands and queries

to run a query and read its response do as follows

```js
bus.ExecuteQuery("GetPayroll","PayrollResponse", { SearchTerms: "*" }).subscribe((v:PayrollResponse) { console.log(v.PayrollId });
```

to execute a command without worrying about a response
```js
bus.ExecuteCommand("SubmitPayroll", { PayrollId: Guid.create() });
```

to execute a command and listen to only the first callback for that command
```js
bus.ExecuteCommand("SubmitPayroll", { PayrollId: Guid.create() }).subscribe((v:CommandData) { console.log("the first response of unkown type")});
```

by setting a callback type you can have that type be the final expected message, and know that you are *somewhat* typesafe

```js
bus.ExecuteCommand("SubmitPayroll", { PayrollId: Guid.create() },"PayrollSubmitionComplete").subscribe((v:PayrollSubmitionComplete) { console.log("the first response of unkown type")});
```

if your command returns multiple types of callback you must define your final callback type and then you can use the statusEvent to subscribe to other callback types; 
```js
let callbackObserver = bus.ExecuteCommandWithStatusUpdates("SubmitPayroll", { PayrollId: Guid.create() },"PayrollSubmitionComplete");
callbackObserver.statusEvent.subscribe((c : Callback<PayrollCallbacks>) => { 
	if (c.Type == "PayrollSubmissionInProgress") {
		let data:PayrollSubmissionInProgress = c.Payload;
		console.log(`Payroll {data.PercentComplete} complete`);
	}
})
callbackObserver.subscribe((v:PayrollSubmitionComplete) { console.log("the first response of unkown type")});
```


if there is a http error from the sending for the command that can be found in the error part of the subscription
```js
let callbackObserver = bus.ExecuteCommandWithStatusUpdates("SubmitPayroll", { PayrollId: Guid.create() },"PayrollSubmitionComplete");

callbackObserver.subscribe(/*next*/()=> {}, /*error*/(err)=> { console.log("Hey some error happened",err});
```

The callback observer for a command is only for that specific instance of the command being executed, it uses some signalr and a correlationId to make sure the messages only go where they should. 
When the commandObserver receives the type defined for the callback it stops listening to signalr messages for that correlationId;



## Testing Module

AttacheBusTestingModule

```js
beforeEach(() => {
	TestBed.configureTestingModule({
		imports: [//...
		AttacheBusTestingModule 
		//... 
		],
		//...
	}).compileComponents();
});

it("Should run some query",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	controller.expectQuery("GetPayroll");
	controller.verify();
})))

it("Should run some specific query",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	tick();
	controller.expectQuery("GetPayroll", { SearchTerms:"expected search term" });
	controller.verify();
})))

it("Should run some specific query, and return a result",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	tick();
	controller.expectQuery("GetPayroll", { SearchTerms:"expected search term" }).returnWith({ PayrollId: Guid.create(); });
	controller.verify();
})))


it("Should run some command",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	controller.expectCommand("SubmitPayroll");
	controller.verify();
})))


it("Should run some specific command",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	tick();
	controller.expectCommand("SubmitPayroll", { PayrollId:"aaaa-bbbbbbbbb-aaaa" });
	controller.verify();
})))

it("Should run some specific command, and return a result",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	tick();
	let cmd = controller.expectCommand("SubmitPayroll", { PayrollId:"aaaa-bbbbbbbbb-aaaa" });
	cmd.sendResponse<PayrollCallbacks>("PayrollSubmitionComplete", { Success: true });
	controller.verify();
})))

it("Should run some specific command, and return multiple results",fakeAsync(inject([/* ... */, BusTestingController], (/* ... */, controller:BusTestingController) =>{ 
	/* ... some testing ... */
	tick();
	controller.expectCommand("SubmitPayroll", { PayrollId:"aaaa-bbbbbbbbb-aaaa" });
	cmd.sendResponse<PayrollCallbacks>("PayrollSubmitionInProgress", { PercentComplete: 0.4 });
	/* ... some testing ... */

	cmd.sendResponse<PayrollCallbacks>("PayrollSubmitionComplete", { Success: true });
	controller.verify();
})))
```