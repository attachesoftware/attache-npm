import { NgModule } from "@angular/core";
import { AttacheCommandQueryBus } from "./bus";
import { SignalRService } from "./signalr.service";
import { HubConnectionBuilder } from "@aspnet/signalr";

@NgModule({
    providers: [SignalRService, AttacheCommandQueryBus,
        HubConnectionBuilder]
})
export class AttacheBusCommonModule {

}
