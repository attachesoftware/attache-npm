export interface Command<CommandType> {
    Type: CommandType;
    Data: string;
}
export interface CommandData {

}