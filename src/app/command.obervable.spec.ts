import { TestBed, getTestBed, ComponentFixture, async } from '@angular/core/testing';
import { CommandObservervable } from './command.observable';
import { CallbackEvent, CallbackData } from './callback';
import { Subject } from 'rxjs';



class MyCallbackData implements CallbackData {
    icecream:string;
    cake:string
}

describe('CommandObservable', () => {    
    let testBed:TestBed;
    let fixture: ComponentFixture<CommandObservervable<MyCallbackData>>;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [,
                  
                
            ]
        });
        testBed = getTestBed()        
    });

    beforeEach(() => {        

    });

    it('should be able to publish expected message type directly', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myboom:MyCallbackData = undefined;
        service.subscribe((boom:MyCallbackData) =>  myboom = boom);
        subject.next({ Type: "MyCallbackData", Payload: { icecream: "some ice cream"} })
        expect(myboom).toBeTruthy();
        expect(myboom.icecream).toBe("some ice cream");
    }));

    it('should be able to publish expecte message type through completeEvent', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myboom:MyCallbackData = undefined;
        service.completeEvent.subscribe((boom:MyCallbackData) =>  myboom = boom);
        subject.next({ Type: "MyCallbackData", Payload: { icecream: "some ice cream"} })
        expect(myboom).toBeTruthy();
        expect(myboom.icecream).toBe("some ice cream");
    }));

    it('should be able to pass http error through iteself expecte message type through iteslfe', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myerror:any = undefined;
        service.subscribe(() => {} ,(boom:any) => { myerror = boom} );
        myhttp.error({wooosh: "some error"});
        expect(myerror).toBeTruthy();
        expect(myerror.wooosh).toBe("some error");
    }));

    it('should be able to complete subject when expected message type is received', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myboom:MyCallbackData = undefined;
        service.subscribe((boom:MyCallbackData) =>  myboom = boom);
        subject.next({ Type: "MyCallbackData", Payload: { icecream: "some ice cream"} })
        let complete = false;
        subject.subscribe(() => {}, () => {}, () => complete = true);
        expect(complete).toBe(true);
    }));

    it('should be able to pass http error through iteself expecte message type through completeEvent', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myerror:any = undefined;
        service.completeEvent.subscribe(() => {} ,(boom:any) => { myerror = boom} );
        myhttp.error({wooosh: "some error"});
        expect(myerror).toBeTruthy();
        expect(myerror.wooosh).toBe("some error");
    }));


    it('should be able to pass http error through iteself expecte message type through iteslfe', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myerror:any = undefined;
        service.subscribe(() => {} ,(boom:any) => { myerror = boom} );
        myhttp.error({wooosh: "some error"});
        expect(myerror).toBeTruthy();
        expect(myerror.wooosh).toBe("some error");
        let complete = false;
        subject.subscribe(() =>{} ,() => {}, () => complete =true);
        expect(complete).toBe(true);
    }));

    it('should send unknown events types te statusEvent', async(() => {
        var subject = new Subject<CallbackEvent<MyCallback>>();
        var myhttp = new Subject<any>();
        let service = new CommandObservervable<MyCallback>("MyCallbackData", subject, myhttp.asObservable());
        let myboom:CallbackEvent<MyCallback> = undefined;
        service.statusEvent.subscribe((boom:CallbackEvent<MyCallback>) =>  myboom = boom);
        subject.next({ Type: "AnotherType", Payload: { icecream: "some ice cream"} })
        expect((myboom.Payload as any).icecream).toBe("some ice cream");
    }));
});

class CallbackClass implements CallbackEvent<MyCallback> 
{
    Type: MyCallback;
    Payload : CallbackData;
}
type MyQuery = 'TestQuery';
type MyResponse = 'MyResponse';
type MyCommand = 'MyCommand';

type MyCallback = 'MyCallbackData' | 'AnotherType';