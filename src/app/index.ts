import { NgModule, ModuleWithProviders } from '@angular/core';
import { API_PATH } from "./configs";
import { AttacheBusCommonModule } from "./module";

export { AttacheCommandQueryBus } from "./bus";
export { Query, QueryData, QueryResponse } from "./query"
export { CallbackEvent, CallbackData} from './callback'
export { Command, CommandData } from './command'
export { CommandObservervable } from './command.observable'
export { SignalRService } from "./signalr.service";

// moved here as AttachBusTestingModule inlcudes AttacheBusModule // 
export { BusTestingController, AttacheBusTestingModule, SignalRTestingController } from "./testing";


@NgModule({
    imports: [ AttacheBusCommonModule ]
})
export class AttacheBusModule {

    static ForApi(endpoint: string = null, onlineEnvironment: string = null): ModuleWithProviders<AttacheBusModule> {
        return {
            ngModule: AttacheBusModule,
            providers: [
                { provide: API_PATH, useValue: endpoint}
            ]
        };
    }
}
