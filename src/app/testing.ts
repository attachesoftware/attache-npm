import { Injectable, NgModule } from "@angular/core";
import { Subject, BehaviorSubject } from "rxjs";
import { Guid } from "guid-typescript";
import { API_PATH } from "./configs";
import { HttpTestingController, HttpClientTestingModule } from "@angular/common/http/testing";
import { AttacheBusCommonModule } from "./module";
import { SignalRService } from "./signalr.service";
import { CallbackEvent } from "./callback";


@Injectable()
export class SignalRTestingController {
    public Subjects: { [id: string]: Subject<any> } = {};
    public currentId:any = new BehaviorSubject(Guid.EMPTY.toString());
    Start() {

    }

    GetSubject<T>(correlationId: Guid) {

        this.Subjects[correlationId.toString()] = new Subject<any>();
        return this.Subjects[correlationId.toString()];

    }
}


interface QueryExpectation {
    returnWith:(data:any) =>void;
}
@Injectable()
export class BusTestingController {
    private path: API_PATH = "testapi"
    constructor(
    private http: HttpTestingController,
    signalr: SignalRService
    ) {
        this.signalR = signalr as any;
    }
    private signalR:SignalRTestingController
    expectCommand(commandName: string, commandData?: any) {
        let correlationId = 'nothing'
        this.http.expectOne((x) => {
            
            if (x.url.endsWith(`api/${this.path}/Command`) && x.method == "POST" && x.body.Type == commandName && (!commandData || x.body.Data == JSON.stringify(commandData))) {
                correlationId = x.headers.get("correlationId");
                return true;
            }
            return false;
        })
        return {
            sendResponse: <T>(type:T, response: any) => {
                let c:CallbackEvent<T> = { Type: type, Payload: response };
                this.signalR.Subjects[correlationId].next(c);
            }

        };
    }

    expectQuery(queryName: string, query?: any):QueryExpectation {
        let myQuery = this.http.expectOne((x) => {
            console.log(x);
            
            return x.url.endsWith(`api/${this.path}/Query`) && x.method == "POST" && x.body.QueryType == queryName && (!query || x.body.Data == JSON.stringify(query));
        })
        return {
            returnWith: (data: any) => {
                myQuery.flush(data);
            }
        };
    }

    verify() {
        this.http.verify();
    }
}


@NgModule({
    imports: [AttacheBusCommonModule, HttpClientTestingModule],
    providers: [
        { provide: SignalRService, useClass: SignalRTestingController },
        { provide: SignalRTestingController, useClass: SignalRTestingController },
        { provide: API_PATH, useValue: 'testapi'},
        { provide: BusTestingController, useClass: BusTestingController }
    ]
})
export class AttacheBusTestingModule {

}
