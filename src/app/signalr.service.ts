import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { CallbackEvent } from './callback';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';

@Injectable({
    providedIn: 'root'
})


export class SignalRService {

    private started = false;

    constructor(private builder: HubConnectionBuilder) {

    }
    
    private _hubConnection: HubConnection;
    private myId = new BehaviorSubject(undefined);
    currentId: Observable<string> = this.myId.asObservable();

    private eventListeners: any = {};
    private observables: {[id:string]:SignalRCallback} = {}    

    get Started() {
        return this.started;
    }

    public Start() {

        const hostname = location.hostname;
        const parts = hostname.split('.');
        let gateway = location.protocol + '//' + hostname.replace(parts[0], 'gateway');

        gateway = gateway.replace('local.attachesoftwaredev.com', 'localtest.me');

        this._hubConnection = this.builder.withUrl(gateway + '/sockets/events')
            .build();

        this._hubConnection
            .start()
            .then(() => {
                console.log('Connection started!');
                this.started = true;
            })
            .catch(err => console.log('Error while establishing connection :( ' + err));

        this._hubConnection.on('YouAre', (id: string) => {
            this.myId.next(id);
            console.log(('I am with id:' + id));
        });

        this._hubConnection.on('OnEvent', (signalREvent: SignalRRelayedEvent) => {
            const correlationId = signalREvent.correlationId;
            const payload = signalREvent.payload;            
                       

            if (this.observables[correlationId]) {
                this.observables[correlationId].callbackSubject.next(this.observables[correlationId].callbackFactory(signalREvent));
            }
           
        });

    }
    public GetSubject<CallbackType>(correlationId: Guid): Subject<CallbackEvent<CallbackType>> {
        
        if (!this.observables[correlationId.toString()]) {
            let mySubject = new Subject<CallbackEvent<CallbackType>>();
            let myCallbackFactory = (evnt: SignalRRelayedEvent):CallbackEvent<CallbackType> => { return { Type: evnt.type as CallbackType, Payload: JSON.parse(evnt.payload)} };
            mySubject.subscribe(null,null, () => 
            {
                this.observables[correlationId.toString()] = null;
            })
            this.observables[correlationId.toString()] = { callbackSubject: mySubject, callbackFactory: myCallbackFactory };
        }
        return this.observables[correlationId.toString()].callbackSubject;
    }

    

}

class SignalRCallback { 
    callbackSubject:Subject<any>;
    callbackFactory: (evnt: SignalRRelayedEvent) => CallbackEvent<any>;
}

class SignalRRelayedEvent {
    recipient: string;
    type: any;
    payload: string;
    correlationId: string;
} 
