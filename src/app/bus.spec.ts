import { TestBed, inject, getTestBed, ComponentFixture, async, flushMicrotasks, fakeAsync, tick } from '@angular/core/testing';
import { AttacheCommandQueryBus } from './bus';
import { SignalRService } from './signalr.service';
import { MockBackend } from '@angular/http/testing'
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { API_PATH } from "./configs";
import { HttpClient } from '@angular/common/http';
import { Subject, BehaviorSubject } from 'rxjs';
import { CommandObservervable } from './command.observable';



class FakeHttp {
    
}

describe('AttachCommandQueryBus', () => {    
    let testBed:TestBed;
    let fixture: ComponentFixture<AttacheCommandQueryBus<MyQuery, MyResponse, MyCommand, MyCallback>>;
    let subject: Subject<any> = new Subject();
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [AttacheCommandQueryBus,
                {
                    provide: SignalRService,
                    useValue: { 
                        GetSubject: () => subject , 
                        currentId: new BehaviorSubject("myId"), 
                        Started: () => true 
                    } 
                },  
                { 
                    provide: API_PATH,
                    useValue: "monkey"
                }              
                
            ]
        });
        testBed = getTestBed()        
    });

    beforeEach(() => {        

    });

    it('should be created', inject([AttacheCommandQueryBus], (service: AttacheCommandQueryBus<MyQuery,MyResponse,MyCommand,MyCallback>) => {
        expect(service).toBeTruthy();
    }));

    it('should ExecuteQuery', inject([AttacheCommandQueryBus], (service: AttacheCommandQueryBus<MyQuery, MyResponse, MyCommand, MyCallback>, httpMock:HttpTestingController) => {
        expect(service).toBeTruthy();
        
        let response = service.ExecuteQuery("TestQuery", "MyResponse", {})
        expect(response).toBeTruthy();
    }));
    it('should work', async(inject([HttpClient, HttpTestingController], (http:HttpClient, httpController:HttpTestingController) => {
        http.get("/mypath").subscribe();
        httpController.expectOne("/mypath");
        
        httpController.verify();

    })));

    it(`should expect a GET /foo/bar`, async(inject([HttpClient, HttpTestingController],
        (http: HttpClient, backend: HttpTestingController) => {
            http.get('/foo/bar').subscribe();

            backend.expectOne({
                url: '/foo/bar',
                method: 'GET'
            });
            backend.verify();
        })));

    it('should ExecuteQuery and hit endpoint', async(inject([HttpTestingController, AttacheCommandQueryBus], ( httpMock: HttpTestingController, service: AttacheCommandQueryBus<MyQuery, MyResponse, MyCommand, MyCallback>,) => {
        
        expect(service).toBeTruthy();    
        let queryBody = { somevalue: "value"}
        let response = service.ExecuteQuery("TestQuery", "MyResponse", queryBody).subscribe()         
        httpMock.expectOne((req) =>        
        { 

            return req.url == "http://gateway/api/monkey/Query" && req.method == "POST" && req.body.QueryType == "TestQuery"
                && req.body.ResponseType == "MyResponse" && req.body.Data == JSON.stringify(queryBody)
        });
        expect(response).toBeTruthy();        
        httpMock.verify();
    })));


    it('should ExecuteCommandWithStatusUpdates and hit endpoint only once', fakeAsync(inject([HttpTestingController, AttacheCommandQueryBus], ( httpMock: HttpTestingController, service: AttacheCommandQueryBus<MyQuery, MyResponse, MyCommand, MyCallback>,) => {
        
        expect(service).toBeTruthy();    
        let queryBody = { somevalue: "value"}
           
        let response:CommandObservervable<MyCallback> = undefined;


        
        response = service.ExecuteCommandWithStatusUpdates("MyCommand", queryBody, "MyCallback"); ;
        response.subscribe(() => {});
        expect(response).toBeTruthy();     
        tick(); 
        
        let post = false;
        
        response.httpPost.subscribe(() => post = true);
        tick(); 
        httpMock.expectOne((req) =>        
        { 
            console.log(req);

            return req.url == "http://gateway/api/monkey/Command" && req.method == "POST" && req.body.Type == "MyCommand"
                && req.body.Data == JSON.stringify(queryBody)
        });
        
        httpMock.verify();        

        expect(post).toBe(true);

    })));

    it('should ExecuteCommandWithStatusUpdates and errors should be passed through httpPost', fakeAsync(inject([HttpTestingController, AttacheCommandQueryBus], ( httpMock: HttpTestingController, service: AttacheCommandQueryBus<MyQuery, MyResponse, MyCommand, MyCallback>,) => {
        
        expect(service).toBeTruthy();    
        let queryBody = { somevalue: "value"}
           
        let response:CommandObservervable<MyCallback> = undefined;

        response = service.ExecuteCommandWithStatusUpdates("MyCommand", queryBody, "MyCallback"); ;
        response.subscribe(() => {});
        expect(response).toBeTruthy();     
        tick();
        
        let error = false;
        
        response.httpPost.subscribe(()=>{},() => error = true);
        
        httpMock.expectOne((req) =>        
        { 
            console.log(req);

            return req.url == "http://gateway/api/monkey/Command" && req.method == "POST" && req.body.Type == "MyCommand"
                && req.body.Data == JSON.stringify(queryBody)
        }).flush("error", {status:500, statusText: "Internal Server Error"});
        try {
            tick();
        } catch  // If I don't do this karma thinks the Internal Server Error is a genuine Error;
        {

        }
        httpMock.verify();                
        expect(error).toBe(true);

    })));

    it('should ExecuteQuery and hit endpoint (default generic parameters)', async(inject([HttpTestingController, AttacheCommandQueryBus], ( httpMock: HttpTestingController, service: AttacheCommandQueryBus) => {
        
        expect(service).toBeTruthy();    
        let queryBody = { somevalue: "value"}
        let response = service.ExecuteQuery("TestQuery", "MyResponse", queryBody).subscribe()         
        httpMock.expectOne((req) =>        
        { 

            return req.url == "http://gateway/api/monkey/Query" && req.method == "POST" && req.body.QueryType == "TestQuery"
                && req.body.ResponseType == "MyResponse" && req.body.Data == JSON.stringify(queryBody)
        });
        expect(response).toBeTruthy();        
        httpMock.verify();
    })));


    it('should ExecuteCommandWithStatusUpdates and hit endpoint only once (default generic parameters)', fakeAsync(inject([HttpTestingController, AttacheCommandQueryBus], ( httpMock: HttpTestingController, service: AttacheCommandQueryBus) => {
        
        expect(service).toBeTruthy();    
        let queryBody = { somevalue: "value"}
           
        let response:CommandObservervable<string> = undefined;



        response = service.ExecuteCommandWithStatusUpdates("MyCommand", queryBody, "MyCallback"); ;
        response.subscribe(() => {});
        expect(response).toBeTruthy();     
        tick(); 
        
        let post = false;
        
        response.httpPost.subscribe(() => post = true);
        tick(); 
        httpMock.expectOne((req) =>        
        { 
            console.log(req);

            return req.url == "http://gateway/api/monkey/Command" && req.method == "POST" && req.body.Type == "MyCommand"
                && req.body.Data == JSON.stringify(queryBody)
        });
        
        httpMock.verify();        

        expect(post).toBe(true);

    })));
});

type MyQuery = 'TestQuery';
type MyResponse = 'MyResponse';
type MyCommand = 'MyCommand';

type MyCallback = 'MyCallback';