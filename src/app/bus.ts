import { HttpClient, HttpHeaders, HttpParams, HttpResponse, HttpErrorResponse, HttpRequest } from "@angular/common/http";
import { CommandData, Command } from "./command";
import { Observable, Subject, BehaviorSubject } from "rxjs";
import { tap, map } from 'rxjs/operators';
import { QueryData, Query, IQueryResponse } from "./query";
import { Guid } from "guid-typescript";
import { CommandObservervable } from "./command.observable";
import { SignalRService } from "./signalr.service";
import { Injectable } from "@angular/core";
import { API_PATH } from "./configs";
import { CallbackData } from "./callback";
import { RequestOptions, Response } from "@angular/http";
import { ifStmt } from "@angular/compiler/src/output/output_ast";
import { Message, IMessageResponse } from "./message";


@Injectable()
export class AttacheCommandQueryBus<QueryType = string, QueryResponseType = string, CommandType = string, CallbackType = string, MessageType = string, MessageData=string> {
    client: any;

    private bearerToken: string;
    private responseInterceptFunction: (response: HttpResponse<any>) => void;
    private errorInterceptFunction: (response: HttpErrorResponse) => void;

    constructor(
        private http: HttpClient,
        private signalRService: SignalRService,
        private apiPath: API_PATH
    ) {
        this.client = this.http;
    }

    public setBearerToken(token: string): void {
        this.bearerToken = token;
    }

    public setResponseInterceptFunction(func: (response: HttpResponse<any>) => void) {
        this.responseInterceptFunction = func;
    }

    public setErrorInterceptFunction(func: (response: HttpErrorResponse) => void) {
        this.errorInterceptFunction = func;
    }

    public ExecuteCommandWithStatusUpdates(commandName: CommandType, commandData: CommandData, callbackEvent: CallbackType = null, endpoint = null, onlineEnvironment = null): CommandObservervable<CallbackType> {
        const correlationId = Guid.create();
        var signalRSubject = this.signalRService.GetSubject<CallbackType>(correlationId);
        let httpPost: Subject<any> = new BehaviorSubject<any>(null);
        const gateway = this.getGateway(onlineEnvironment);

        if (!this.signalRService.Started) {
            this.signalRService.Start();
        }

        this.waitForId()
            .then(signalRId => {

                const requestOptions = this.generateRequestOptionsWithSignalR(signalRId, correlationId);
                const command = this.generateCommand(commandName, commandData);
                this.http.post(`${gateway}/api/${endpoint || this.apiPath}/Command`, command, requestOptions)
                    .subscribe((next: any) => httpPost.next(next), (error: any) => httpPost.error(error), () => httpPost.complete());
            });

        return new CommandObservervable(callbackEvent, signalRSubject, httpPost);
    }

    public ExecuteCommand(commandName: CommandType, commandData: CommandData, callbackEvent: CallbackType = null, endpoint = null, onlineEnvironment = null): Observable<CallbackData> {

        const correlationId = Guid.create();
        var signalRSubject = this.signalRService.GetSubject<CallbackType>(correlationId);
        let httpPost: Subject<any> = new BehaviorSubject<any>(null);
        const gateway = this.getGateway(onlineEnvironment);

        const command = this.generateCommand(commandName, commandData);

        if (callbackEvent) {

            if (!this.signalRService.Started) {
                this.signalRService.Start();
            }

            this.waitForId()
                .then(signalRId => {
                    const requestOptions = this.generateRequestOptionsWithSignalR(signalRId, correlationId);

                    this.http.post(`${gateway}/api/${endpoint || this.apiPath}/Command`, command, requestOptions)
                        .pipe(
                            tap((response: HttpResponse<any>) => {
                                if (this.responseInterceptFunction) {
                                    this.responseInterceptFunction(response);
                                }
                            }, err => {
                                if (this.errorInterceptFunction) {
                                    this.errorInterceptFunction(err);
                                }
                            }),
                            map((response: HttpResponse<any>) => response.body)
                        )
                        .subscribe((next: any) => httpPost.next(next), (error: any) => httpPost.error(error), () => httpPost.complete());

                });

            const observable = new Observable((observer) => {
                signalRSubject.subscribe(
                    (item) => {
                        if (item.Type == callbackEvent) {
                            observer.next(item.Payload);
                            observer.complete();
                            signalRSubject.complete();                  
                        }
                    });
                httpPost.subscribe(() => { }, (err: any) => { signalRSubject.complete(); observer.error(err); });
            });

            return observable;
        } else {
            const observable = this.http.post(`${gateway}/api/${endpoint || this.apiPath}/Command`, command, this.generateRequestOptions())
            .pipe(
                tap((response: HttpResponse<any>) => {
                    if (this.responseInterceptFunction) {
                        this.responseInterceptFunction(response);
                    }
                }, err => {
                    if (this.errorInterceptFunction) {
                        this.errorInterceptFunction(err);
                    }
                }),
                map((response: HttpResponse<any>) => response.body)
            );

            return observable;
        }
    }


    public ExecuteQuery(queryName: QueryType, responseName: QueryResponseType, data: QueryData, endpoint = null, onlineEnvironment = null): Observable<IQueryResponse> {

        const gateway = this.getGateway(onlineEnvironment);

        const query = this.generateQuery(queryName, responseName, data);

        const observable = (this.http.post(
            `${gateway}/api/${endpoint || this.apiPath}/Query`, query, { withCredentials: true, observe: 'response' }) as Observable<IQueryResponse>)
            .pipe(
                tap((response: HttpResponse<any>) => {
                    if (this.responseInterceptFunction) {
                        this.responseInterceptFunction(response);
                    }
                }, err => {
                        if (this.errorInterceptFunction) {
                            this.errorInterceptFunction(err);
                        }
                }),
                map((response: HttpResponse<any>) => response.body)
            );

        return observable;
    }

    public SendMessage(messageType: MessageType, data: MessageData, endpoint = null, onlineEnvironment = null): Observable<IMessageResponse> {
        const gateway = this.getGateway(onlineEnvironment);

        const message = this.generateMessage(messageType, data);

        const observable = (this.http.post(
            `${gateway}/api/${endpoint || this.apiPath}/Message`, message, { withCredentials: true, observe: 'response' }) as Observable<IMessageResponse>)
            .pipe(
                tap((response: HttpResponse<any>) => {
                    if (this.responseInterceptFunction) {
                        this.responseInterceptFunction(response);
                    }
                }, err => {
                        if (this.errorInterceptFunction) {
                            this.errorInterceptFunction(err);
                        }
                }),
                map((response: HttpResponse<any>) => response.body)
            );

        return observable;
    }

    private waitForId(): Promise<any> {
        return new Promise(resolve => {
            this.signalRService.currentId.subscribe(id => {
                if (id != null) {
                    resolve(id);
                }
            });
        });
    }

    private generateRequestOptionsWithSignalR(signalRId: string, correlationId: Guid): IRequestOptions {
        const header = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'RespondTo': signalRId,
            'CorrelationId': correlationId.toString()
        };

        if (this.bearerToken) {
            header['Authorization'] = 'Bearer ' + this.bearerToken;
        }

        return {
            headers: new HttpHeaders(header),
            withCredentials: true,
            observe: 'response'
        };
    }

    private generateRequestOptions(): IRequestOptions {
        const header = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type'
        };

        if (this.bearerToken) {
            header['Authorization'] = 'Bearer ' + this.bearerToken;
        }

        return {
            headers: new HttpHeaders(header),
            observe: 'response',
            withCredentials: true,
        };
    }

    private getGateway(onlineEnvironment: string = null) {
        if (onlineEnvironment) {
            const protocol = onlineEnvironment === 'localtest.me' ? 'http' : 'https';
            return protocol + '://gateway.' + onlineEnvironment;
        }
        const hostname = location.hostname;
        const parts = hostname.split('.');
        return location.protocol + '//' + hostname.replace(parts[0], 'gateway');
    }

    private generateCommand(commandName: CommandType, commandData: CommandData) {
        const command: Command<CommandType> = {
            Type: commandName,
            Data: JSON.stringify(commandData)
        };

        return command;
    }

    private generateQuery(queryName: QueryType, responseName: QueryResponseType, queryData: QueryData) {
        const query: Query<QueryType, QueryResponseType> = {
            QueryType: queryName,
            ResponseType: responseName,
            Data: JSON.stringify(queryData)
        };

        return query;
    }

    private generateMessage(messageType: MessageType, messageData: MessageData) {
        const query: Message<MessageType> = {
            Type: messageType,
            Data: JSON.stringify(messageData)
        };

        return query;
    }
 
}


export interface IRequestOptions {
    body?: any;
    headers?: HttpHeaders | { [header: string]: string | Array<string> };
    observe?: any;
    params?: HttpParams | { [param: string]: string | Array<string> };
    reportProgress?: boolean;
    responseType?: "json";
    withCredentials?: boolean;
}