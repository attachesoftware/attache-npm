export interface Message<MessageType> {
    Type: MessageType;
    Data: string;
}

export interface IMessageResponse {

}
export class MessageResponse implements IMessageResponse {
    Message: string;
}