export interface Query<QueryType, QueryResponse> {
    QueryType: QueryType;
    ResponseType: QueryResponse;
    Data: string;
}

export interface QueryData {

}
export interface IQueryResponse {

}
export class QueryResponse implements IQueryResponse {
    Successful: boolean;
    Message: string;
}
