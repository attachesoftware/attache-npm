import { Observable, Subject } from "rxjs";
import { CallbackData, CallbackEvent } from "./callback";

export class CommandObservervable<CallbackType> extends Observable<CallbackData> {    
    constructor(callbackType: CallbackType, successCallback: Subject<CallbackEvent<CallbackType>>, public httpPost:Observable<any>) {
        super((observer) => { 
            successCallback.subscribe(
                (item) => {
                    // if the user specifies a type we fire off our update.
                    // if the callback type is not defined we assume that any message is the final message.
                    if (item.Type == callbackType || callbackType == undefined) { 
                        observer.next(item.Payload);
                        observer.complete(); 
                        successCallback.complete();     // we send that we are completed with the callbacks of this command //                   
                    } 
            });
            httpPost.subscribe(() => {},  (err: any) => { successCallback.complete(); observer.error(err); });
        });
        
        successCallback.subscribe((item) => {
            // if the message type does not equal the callbackType then we fire a status event // 
            if (item.Type != callbackType) {
                this._statusEvent.next(item);
            }
        });

        
        this.completeEvent =  this;
        this._statusEvent = new Subject<CallbackEvent<CallbackType>>();
        this.statusEvent = this._statusEvent;
    }    
    completeEvent: Observable<CallbackData>;
    private _statusEvent: Subject<CallbackEvent<CallbackType>>;
    statusEvent: Observable<CallbackEvent<CallbackType>>;

}