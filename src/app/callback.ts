

export interface CallbackEvent<CallbackType> {

    Type: CallbackType;
    Payload: CallbackData;
}


export interface CallbackData {

}