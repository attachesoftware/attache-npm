import { TestBed, inject, async } from '@angular/core/testing';
import { SignalRService } from './signalr.service';
import { Guid } from 'guid-typescript';
import { CallbackEvent } from './callback';
import { HubConnectionBuilder } from '@aspnet/signalr';


class FakeSignalRBuilder {
    
    public started:boolean;
    public Url:string;
    public listeners:{[id:string]:(...args:any)=>void} = {};
    build() {
        return this;
    }
    withUrl(url:string)
    {
        this.Url = url;
        return this;
    }
    on(funcName: string, func: (...args: any) => void) {
         this.listeners[funcName] = func
    }
    start() {
        this.started =true;
        return Promise.resolve("boop");
    }

}

describe('SignalR', () => {
    let hubBuilder: FakeSignalRBuilder = new FakeSignalRBuilder();    

    beforeEach(() => {
        hubBuilder.started = false;
        TestBed.configureTestingModule({
            providers: [SignalRService,
                { provide: HubConnectionBuilder, useValue: hubBuilder },

            ]
        });
    });

    it('should be created', inject([SignalRService], (service: SignalRService) => {
        expect(service).toBeTruthy();
    }));

    it('should be able to create a subject', inject([SignalRService], (service: SignalRService) => {
        expect(service).toBeTruthy();
        expect(service.GetSubject<string>(Guid.create())).toBeTruthy();
    }));

    it('should be able to start', inject([SignalRService], (service: SignalRService) => {
        expect(service).toBeTruthy();
        service.Start();
        expect(hubBuilder.started).toBe(true);
    }));

    it('should be able to start and wireup YouAre, and OnEvent listeners', inject([SignalRService], (service: SignalRService) => {
        expect(service).toBeTruthy();
        service.Start();
        expect(hubBuilder.started).toBe(true);
        expect(hubBuilder.listeners["OnEvent"]).toBeTruthy()
        expect(hubBuilder.listeners["YouAre"]).toBeTruthy()
    }));

    it('should be able to start handle YouAre Events', async(inject([SignalRService], (service: SignalRService) => {
        expect(service).toBeTruthy();
        service.Start();
        expect(hubBuilder.started).toBe(true);
        expect(hubBuilder.listeners["OnEvent"]).toBeTruthy()
        expect(hubBuilder.listeners["YouAre"]).toBeTruthy()
        let result:string;
        service.currentId.subscribe((s) => result = s);
        hubBuilder.listeners["YouAre"]("myteststring");
        expect(result).toBe("myteststring");
    })));

    it('should be able to start handle OnEvent Events', async(inject([SignalRService], (service: SignalRService) => {
        expect(service).toBeTruthy();
        service.Start();
        expect(hubBuilder.started).toBe(true);
        expect(hubBuilder.listeners["OnEvent"]).toBeTruthy()
        expect(hubBuilder.listeners["YouAre"]).toBeTruthy()        
        var correlationId: Guid = Guid.create();
        var callback: CallbackEvent<MyCallbacks> = null;
        service.GetSubject<MyCallbacks>(correlationId).subscribe((c) => callback = c);        
        hubBuilder.listeners["OnEvent"]({
            recipient: "unkown",
            type: "someType",
            payload: `{ "item1": "strings", "item2": 5 }`,
            correlationId: correlationId.toString()
        } );
        expect(callback.Type).toBe("someType");
        let data = callback.Payload as MyType;
        expect(data.item1).toBe("strings");
        expect(data.item2  + 1).toBe(6);
    })));

});

type MyCallbacks = "someType"
class MyType {
    item1:string;
    item2:number;
}